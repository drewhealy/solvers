The app is run on port 8080
- connects to localstack which is a local instance of AWS used in this case as S3

- to run localstack there is a docker-compose.yml that has the config needed
    to start run: docker-compose up solvers-localstack

- connects to a postgres instance on port 5432 and the database is called verycoolapp

- I would usually set up a flyway migrator to handle the database schema changes but for this
setting spring.jpa.hibernate.ddl-auto=update works fine

- intended DB_Schema can be found in the setUp/DB folder

- get and post uml diagrams can be found in the diagrams folder

- postman collection of the request can be found in setUp/postman

- no unit tests written (not happy about this but ran out of time)

- had the idea to save the thumbnail image in S3 and have it relate back to the post.
    have it working in upload, didn't return the content in GET

- POST request should take in one object for the post data, json mapping just wasn't playing ball
