package com.tech.solvers.controllers;

import com.tech.solvers.models.api.GetResponse;
import com.tech.solvers.models.api.UploadRequest;
import com.tech.solvers.models.api.UploadResponse;
import com.tech.solvers.services.Orchestrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller()
public class VeryCoolController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VeryCoolController.class);

    private final Orchestrator orchestrator;

    @Autowired
    public VeryCoolController(Orchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }

    @GetMapping("/allPosts")
    public ResponseEntity getAllFiles() {
        LOGGER.info("getAllFiles");
        List<GetResponse> files = orchestrator.getAllFiles();
        return new ResponseEntity(files, HttpStatus.OK);
    }

    // should have the UploadRequest as the requestParam, don't have time to fix the mapping
    @PostMapping("/upload")
    public ResponseEntity uploadFileAndMetadata(@RequestParam("description") String description,
                                                @RequestParam("link") String link,
                                                @RequestParam("file") MultipartFile multipartFile) {
        UploadRequest uploadRequest = new UploadRequest(description, link);
        UploadResponse fileBaseResponse = orchestrator.uploadFileAndPost(uploadRequest, multipartFile);
        return new ResponseEntity(fileBaseResponse, HttpStatus.CREATED);
    }
}
