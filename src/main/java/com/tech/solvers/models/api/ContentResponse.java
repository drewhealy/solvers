package com.tech.solvers.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Getter
@RequiredArgsConstructor
public class ContentResponse {

    @NotNull
    @JsonProperty
    private final byte[] content;

    @JsonIgnore
    private String mimeType;

    @JsonIgnore
    private String id;
}
