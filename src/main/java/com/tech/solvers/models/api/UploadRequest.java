package com.tech.solvers.models.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UploadRequest {

    @NotNull
    @JsonProperty
    private String description;

    @NotNull
    @JsonProperty
    private String link;

}
