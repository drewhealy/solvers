package com.tech.solvers.models.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tech.solvers.models.entity.Post;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class GetResponse {

    private int id;

    private String link;

    private String description;

    private String s3BucketName;

    private String s3FileKey;

    private int numberOfComments;

    private int numberOfLikes;

    @Override
    public String toString() {
        return "GetResponse {" +
                "id: " + id + '\'' +
                "link: " + link + '\'' +
                "description: " + description + '\'' +
                "s3BucketName: " + s3BucketName + '\'' +
                "s3FileKey: " + s3BucketName + '\'' +
                "numberOfComments: " + numberOfComments + '\'' +
                "numberOfLikes: " + numberOfLikes + '\'' +
                "}";
    }
}
