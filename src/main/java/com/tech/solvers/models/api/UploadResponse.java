package com.tech.solvers.models.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UploadResponse {

    @NotNull
    @JsonProperty
    private int fileId;

    @NotNull
    @JsonProperty
    private String message;
}
