package com.tech.solvers.models.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
@Getter @Setter @ToString
public class Comments {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "commentUserId")
    private int commentUserId;

    @Column(name = "postId")
    private int postId;

    @Column(name = "comment")
    private String comment;

}
