package com.tech.solvers.models.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;

@Entity
@Table
@Getter @Setter @ToString
public class Post {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "url_link")
    private String link;

    @Column(name = "description")
    private String description;

    @Column(name = "s3bucketname")
    private String s3BucketName;

    @Column(name = "s3filekey")
    private String s3FileKey;

}
