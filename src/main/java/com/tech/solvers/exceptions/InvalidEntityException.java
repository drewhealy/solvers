package com.tech.solvers.exceptions;

public class InvalidEntityException extends RuntimeException {

    public InvalidEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
