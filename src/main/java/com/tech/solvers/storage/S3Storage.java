package com.tech.solvers.storage;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import com.tech.solvers.exceptions.EntityNotFoundException;
import com.tech.solvers.models.FileS3Identifier;
import com.tech.solvers.models.api.ContentResponse;
import com.tech.solvers.models.entity.Post;
import org.apache.tika.exception.TikaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
import org.apache.tika.mime.MediaType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.tech.solvers.utils.Utilities.getMediaType;

public class S3Storage {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3Storage.class.getName());

    private final SimpleDateFormat folderFormatter = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss/");
    private final AmazonS3 amazonS3;

    @Value("${aws.s3.endpoint}")
    private String awsS3Endpoint;

    @Value("${aws.s3.files.bucket.name}")
    private String awsBucketName;

    public S3Storage(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    public FileS3Identifier storeFile(Post post, MultipartFile multipartFile, int fileId) throws IOException {
        ObjectMetadata metaData = buildObjectMetadata(new ObjectMetadata(), post);
        metaData.setContentEncoding(StandardCharsets.UTF_8.name());
        metaData.setContentLength(multipartFile.getSize());

        try {
            MediaType mediaType = getMediaType(multipartFile.getInputStream(), multipartFile.getOriginalFilename());
            metaData.setContentType(mediaType.toString());
        } catch (TikaException e) {
            LOGGER.error("Error determining media type for file: {}. Defaulting to AWS determined media type", fileId);
        }

        String s3ObjectId = createPrefixedFileID(fileId, new Date());
        PutObjectRequest putObjectRequest = new PutObjectRequest(awsBucketName, s3ObjectId, new ByteArrayInputStream(multipartFile.getBytes()), metaData);

        amazonS3.putObject(putObjectRequest);

        return new FileS3Identifier(putObjectRequest.getBucketName(), putObjectRequest.getKey());
    }

    // not used but started work on retrieving file from S3
    public ContentResponse retrieveFileContent(String s3BucketName, String s3Key, String id) throws IOException {

        S3Object s3Object = getFileObject(s3BucketName, s3Key);
        byte[] byteArray = IOUtils.toByteArray(s3Object.getObjectContent());

        String contentResponseString = s3Object.getObjectMetadata().getContentType();
        ContentResponse contentResponse;
        // Get the actual content type of the application octet stream
        if (org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE.equals(contentResponseString)){
            LOGGER.info("S3 returned APPLICATION_OCTET_STREAM_VALUE determining content response from byte array");
            contentResponse =  getUpdatedContentResponse(s3Key, byteArray, contentResponseString, id);
        } else {
            contentResponse = new ContentResponse(byteArray, contentResponseString, id);
        }
        return contentResponse;
    }

    private String createPrefixedFileID(int fileId, Date receivedDate) {
        String formattedDate = folderFormatter.format(receivedDate);
        return formattedDate + fileId;
    }

    private ObjectMetadata buildObjectMetadata(ObjectMetadata metaData, Post post) {
        metaData.addUserMetadata("description", post.getDescription());
        return metaData;
    }

    private ContentResponse  getUpdatedContentResponse(String s3Key, byte[] content, String defaultContentType, String id){
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content);
            String contentType = getMediaType(byteArrayInputStream, s3Key).toString();
            return new ContentResponse(content, contentType, id);
        } catch (IOException | TikaException ex) {
            return new ContentResponse(content, defaultContentType, id);
        }
    }

    private S3Object getFileObject(String s3BucketName, String s3Key) {
        if (amazonS3.doesObjectExist(s3BucketName, s3Key)) {
            return amazonS3.getObject(s3BucketName, s3Key);
        }
        else {
            throw new EntityNotFoundException("Could not retrieve file from S3 bucket " + s3BucketName + " with key " + s3Key);
        }
    }
}
