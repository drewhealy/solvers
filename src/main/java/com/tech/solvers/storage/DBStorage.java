package com.tech.solvers.storage;

import com.tech.solvers.exceptions.EntityNotFoundException;
import com.tech.solvers.models.FileS3Identifier;
import com.tech.solvers.models.entity.Post;
import com.tech.solvers.repository.*;
import com.tech.solvers.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DBStorage {

    // didn't finish the integration with the other 2 repositories
    private final AccountRepository accountRepository;
    private final BoardRepository boardRepository;
    private final CommentsRepository commentsRepository;
    private final LikesRepository likesRepository;
    private final PostRepository postRepository;

    @Autowired
    public DBStorage(AccountRepository accountRepository,
                     BoardRepository boardRepository,
                     CommentsRepository commentsRepository,
                     LikesRepository likesRepository,
                     PostRepository postRepository) {
        this.accountRepository = accountRepository;
        this.boardRepository = boardRepository;
        this.commentsRepository = commentsRepository;
        this.likesRepository = likesRepository;
        this.postRepository = postRepository;
    }

    public List<Post> retrieveFileByNew() {
        return postRepository.findAll();
    }

    public Post savePostData(Post post) {
        return postRepository.save(post);
    }

    public Post updatePostData(int id, FileS3Identifier fileS3Identifier) {
        Optional<Post> optionalPost = postRepository.findById(id);

        if(optionalPost.isPresent()) {
            Post updatedPost = ModelMapper.updateEntityWithS3Identifier(optionalPost.get(), fileS3Identifier);
            return postRepository.save(updatedPost);
        } else {
            throw new EntityNotFoundException("File not found : " + id);
        }
    }

    public int getNumberOfLikes(int id) {
        return likesRepository.countByPostId(id);
    }

    public int getNumberOfComments(int id) {
        return commentsRepository.countByPostId(id);
    }

    public int getNextPostIndex() {
        return (int) postRepository.count() + 1;
    }
}
