package com.tech.solvers.services;

import com.tech.solvers.models.api.GetResponse;
import com.tech.solvers.models.api.UploadRequest;
import com.tech.solvers.models.api.UploadResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Component
public class Orchestrator {

    private final Service service;

    @Autowired
    public Orchestrator(Service service) {
        this.service = service;
    }

    public List<GetResponse> getAllFiles() {
        return service.getAllFiles();
    }

    public UploadResponse uploadFileAndPost(UploadRequest uploadRequest, MultipartFile multipartFile) {
        //TODO: add in uploadRequest validation
        return service.uploadFileAndPost(uploadRequest, multipartFile);
    }


}