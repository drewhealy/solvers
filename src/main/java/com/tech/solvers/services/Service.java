package com.tech.solvers.services;

import com.tech.solvers.controllers.VeryCoolController;
import com.tech.solvers.exceptions.InvalidEntityException;
import com.tech.solvers.models.FileS3Identifier;
import com.tech.solvers.models.api.GetResponse;
import com.tech.solvers.models.api.UploadRequest;
import com.tech.solvers.models.api.UploadResponse;
import com.tech.solvers.models.entity.Post;
import com.tech.solvers.storage.DBStorage;
import com.tech.solvers.storage.S3Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.tech.solvers.utils.ModelMapper.postToGetResponse;

@Component
public class Service {

    private final DBStorage dbStorage;
    private final S3Storage s3Storage;

    private final String STORED = "STORED";

    @Autowired
    public Service(DBStorage dbStorage, S3Storage s3Storage) {
        this.dbStorage = dbStorage;
        this.s3Storage = s3Storage;
    }

    public List<GetResponse> getAllFiles () {

        List<Post> postList = dbStorage.retrieveFileByNew();

        List<GetResponse> responses = postList.stream()
                .map(post -> { return postToGetResponse(post, dbStorage.getNumberOfComments(post.getId()), dbStorage.getNumberOfLikes(post.getId()));
                }).collect(Collectors.toList());

        return responses;
    }

    @Transactional
    public UploadResponse uploadFileAndPost(UploadRequest uploadRequest, MultipartFile multipartFile) {
        try {
            int fileId = dbStorage.getNextPostIndex();
            Post post = new Post();
            post.setId(fileId);
            post.setDescription(uploadRequest.getDescription());
            post.setLink(uploadRequest.getLink());

            dbStorage.savePostData(post);
            FileS3Identifier fileS3Identifier = s3Storage.storeFile(post, multipartFile, fileId);
            dbStorage.updatePostData(fileId, fileS3Identifier);

            return UploadResponse.builder()
                    .fileId(fileId)
                    .message(STORED)
                    .build();
        } catch (IOException e) {
            throw new InvalidEntityException("Invalid metadata provided", e);
        }
    }
}
