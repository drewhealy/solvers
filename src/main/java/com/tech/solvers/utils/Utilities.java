package com.tech.solvers.utils;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;

import java.io.IOException;
import java.io.InputStream;

public final class Utilities {

    public static MediaType getMediaType(InputStream inputStream, String fileName) throws IOException, TikaException {
        TikaConfig tika = new TikaConfig();
        Metadata tikaMetadata = new Metadata();
        tikaMetadata.add(Metadata.RESOURCE_NAME_KEY, fileName);
        return tika.getDetector().detect(TikaInputStream.get(inputStream), tikaMetadata);
    }

}
