package com.tech.solvers.utils;

import com.tech.solvers.models.FileS3Identifier;
import com.tech.solvers.models.api.GetResponse;
import com.tech.solvers.models.entity.Post;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ModelMapper {

    public static Post updateEntityWithS3Identifier(Post postEntity, FileS3Identifier fileS3Identifier) {

        BeanUtils.copyProperties(fileS3Identifier, postEntity);
        return postEntity;
    }

    public static GetResponse postToGetResponse(Post post, int numberOfComments, int numberOfLikes) {
        GetResponse tempGetResponse = new GetResponse();

        BeanUtils.copyProperties(post, tempGetResponse);
        tempGetResponse.setNumberOfComments(numberOfComments); // dbStorage.getNumberOfComments(tempGetResponse.getId())
        tempGetResponse.setNumberOfLikes(numberOfLikes); // dbStorage.getNumberOfLikes(tempGetResponse.getId())

        return tempGetResponse;
    }
}
