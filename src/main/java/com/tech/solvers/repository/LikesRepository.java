package com.tech.solvers.repository;

import com.tech.solvers.models.entity.Likes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikesRepository extends JpaRepository<Likes, Integer> {

    int countByPostId(int postId);
}
