package com.tech.solvers.configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.tech.solvers.storage.S3Storage;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("com.tech.solvers")
@EnableTransactionManagement
@NoArgsConstructor
public class ApplicationConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfig.class);
    private static final Regions region = Regions.EU_WEST_1;

    @Value("${aws.access.key}")
    private String awsAccessKey;

    @Value("${aws.secret.key}")
    private String awsSecretKey;

    @Value("${aws.s3.endpoint}")
    private String awsS3Endpoint;

    @Bean
    public S3Storage fileStorage() {
        AmazonS3 amazonS3 = amazonS3();
        return new S3Storage(amazonS3);
    }

    private AmazonS3 amazonS3() {

        AmazonS3ClientBuilder amazonS3Builder = AmazonS3ClientBuilder.standard()
                .enablePathStyleAccess()
                .disableChunkedEncoding();

        if (!Strings.isBlank(awsAccessKey) && !Strings.isBlank(awsSecretKey)) {
            LOGGER.info("AmazonS3 Client set up with credentials using access key and secret key");
            amazonS3Builder
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)));
        } else {
            LOGGER.info("AmazonS3 Client set up with IAM policies");
            amazonS3Builder.withCredentials(new InstanceProfileCredentialsProvider(true));
        }

        if (awsS3Endpoint != null && !awsS3Endpoint.trim().isEmpty()) {
            LOGGER.info("AmazonS3 Client set up with endpoint: {}", awsS3Endpoint);
            amazonS3Builder
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(awsS3Endpoint, region.getName()));
        } else {
            LOGGER.info("AmazonS3 Client set up without endpoint settings");
            amazonS3Builder.withRegion(region);
        }

        return amazonS3Builder.build();
    }

}
