package com.tech.solvers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SolversApplication {

	@GetMapping("/")
	public String home() {
		return "Solvers APIs";
	}

	public static void main(String[] args) {
		SpringApplication.run(SolversApplication.class, args);
	}

}
